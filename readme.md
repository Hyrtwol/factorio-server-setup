# Factorio Server Setup

Factorio 0.16.36 (alpha)

Default UDP port ```34197```

Server Endpoint: ```factorio.northeurope.cloudapp.azure.com:34197```

## Pre-Install

### SSH setup

Make a host entry in the user ssh config file: ```%USERPROFILE%/.ssh/config```

    Host factorio
        HostName factorio.northeurope.cloudapp.azure.com
        IdentityFile ~/.ssh/id_rsa

Connect command

    ssh tlc@factorio

### Install xz-utils

    sudo apt-get install xz-utils

## Download and unpack game

    wget -O factorio_headless.tar.xz https://www.factorio.com/get-download/0.16.36/headless/linux64
    cd /opt
    sudo tar -xJf ~/factorio_headless.tar.xz
    rm ~/factorio_headless.tar.xz

### Create game folders

    sudo mkdir /opt/factorio/saves
    sudo mkdir /opt/factorio/mods

## Create user

    sudo adduser --disabled-login --no-create-home --gecos factorio factorio
    sudo chown -R factorio:factorio /opt/factorio

## Create settings

    sudo -u factorio cp /opt/factorio/data/server-settings.example.json /opt/factorio/data/server-settings.json

## Create save game

    sudo -u factorio /opt/factorio/bin/x64/factorio --create /opt/factorio/data/saves/toms.zip
    sudo -u factorio /opt/factorio/bin/x64/factorio --start-server /opt/factorio/data/saves/toms.zip

Or download a premade

    sudo -u factorio wget -O /opt/factorio/data/saves/FirstGo.zip http://hyrtwol.dk/factorio/FirstGo.zip

## Start the server

    sudo -u factorio /opt/factorio/bin/x64/factorio --start-server /opt/factorio/data/saves/toms.zip

## Run as a service

    sudo nano /etc/systemd/system/factorio.service

### Service description: ```factorio.service```

```
[Unit]
Description=Factorio Headless Server

[Service]
Type=simple
User=factorio
ExecStart=/opt/factorio/bin/x64/factorio --start-server /opt/factorio/data/saves/toms.zip --server-settings /opt/factorio/data/server-settings.json

[Install]
WantedBy=multi-user.target
```

To load latest replace ```--start-server <save-file>``` with ```--start-server-load-latest```

### System Control

#### Daemon Reload:

    sudo systemctl daemon-reload

#### Enable service auto start:

    sudo systemctl enable factorio

#### Check if service is enabled:

    sudo systemctl is-enabled factorio

#### Start:

    sudo systemctl start factorio

#### Stop:

    sudo systemctl stop factorio

#### Status:

    sudo systemctl status factorio

#### Restart:

    sudo systemctl restart factorio

### Show log:

    cat /opt/factorio/factorio-current.log

### Edit settings:

    sudo nano /opt/factorio/data/server-settings.json

## Resources

* https://wiki.factorio.com/Multiplayer
* https://www.home-assistant.io/docs/autostart/systemd/
